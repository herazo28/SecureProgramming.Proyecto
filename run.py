# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, session, redirect, url_for
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import uuid
import sys
from werkzeug.utils import secure_filename
import onetimepass as otp
import MySQLdb as mdb
from flask_wtf.csrf import CsrfProtect

app = Flask(__name__)
csrf = CsrfProtect()
csrf.init_app(app)
UPLOAD_FOLDER = app.root_path + '/plain'
ALLOWED_EXTENSIONS = set(['txt'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

con = None
 
# MySQL configurations

app.secret_key = os.urandom(24)

@app.route("/", methods=['POST', 'GET'])
def login():
    
    nombre = 'parametros.txt'
    archivo=open(nombre, 'r')	#observese la “r”
    salida = archivo.readlines()	#le ordenamos leer el archivo
    archivo.close()			#cerramos	
    con = mdb.connect('localhost', salida[0].replace('\n', ''), salida[1].replace('\n', ''), salida[2].replace('\n', ''))
    
    #devolvemos el resultado    
    
    
    
    if request.method == 'GET':
        return render_template('index.html')
    elif request.method == 'POST':
        name = request.form['name']
        password = request.form['contrasena']
        
        with con:    
            cur = con.cursor()
                
            cur.execute('''SELECT IFNULL(tipo_usuario, 0), nombre, autorizado_por, identificacion
                    FROM Banco.usuario
                    WHERE usuario = %s
                    AND contrasena = %s;''',(name, password))              
                    
            rows = cur.fetchall()        
        
        if len(rows) != 1:
            return render_template('index.html', mensajeError='El usuario y/o contraseña esta erroneo')
        
        perfil = (rows[0])[0]
        autorizado_por = (rows[0])[2]
        identificacion = (rows[0])[3]
        
        menu = []        
        if perfil == 1:
            session['user'] = name
            session['perfil'] = 1
            session['identificacion'] = identificacion
            menu.append({'tipo': 'app', 'valor': 'Habilitar usuarios', 'icono': 'glyphicon-user', 'url': '/habilitarUsuarios'})
            menu.append({'tipo': 'app', 'valor': 'Ver transacciones a aprobar', 'icono': 'glyphicon-random', 'url': '/TransaccionesFuncionario'})
            menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
            
            return render_template('Menu.html', menu=menu)
        elif perfil == 2 and autorizado_por == None:
            return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Nos encontramos validando su información, cuando la plataforma esté disponible recibirá un correo. Muchas gracias')
        elif perfil == 2 and autorizado_por != None:
            session['user'] = name
            session['identificacion'] = identificacion
            session['perfil'] = 2
            
            menu.append({'tipo': 'app', 'valor': 'Transferir dinero', 'icono': 'glyphicon-random', 'url': '/transferirDinero'})
            menu.append({'tipo': 'app', 'valor': 'Ver transacciones', 'icono': 'glyphicon-blackboard', 'url': '/TransaccionesUsuario'})
            menu.append({'tipo': 'app', 'valor': 'SubirArchivo', 'icono': 'glyphicon-blackboard', 'url': '/subirArchivo'})
            menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
            return render_template('Menu.html', menu=menu)
        
        
    
@app.route("/signup", methods=['POST', 'GET'])
def signup():
    if request.method == 'POST':
        nombre = request.form['nombre']
        nombreUsuario = request.form['nombreUsuario']
        identificacion = request.form['identificacion']
        email = request.form['email']
        password = request.form['password']
        
        with con:    
            cur = con.cursor()
                
            cur.execute('''insert into usuario (  identificacion, 
                                      usuario, 
                                      tipo_usuario, 
                                      nombre, 
                                      contrasena, 
                                      mail) 
                                      values 
                                      (%s, 
                                       %s, 
                                        2, 
                                       %s, 
                                       %s, 
                                       %s
                                       )''',(identificacion, nombreUsuario, nombre, password, email))
                    
            cur.fetchall()        
        
            con.commit()

        return render_template('mensaje.html', Encabezado='Muchas gracias por registrarse en Banco Seguro', Mensaje='Estamos validando su información, cuando termine el proceso se le enviará un correo electrónico')
    elif request.method == 'GET':
        return render_template('signup.html')

@app.route("/habilitarUsuarios")
def pageHabilitarUsuarios():
    
    if 'perfil' not in session or session['perfil'] != 1:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')
    
    menu = []
    menu.append({'tipo': 'app', 'valor': 'Habilitar usuarios', 'icono': 'glyphicon-user', 'url': '/habilitarUsuarios'})
    menu.append({'tipo': 'app', 'valor': 'Ver transacciones a aprobar', 'icono': 'glyphicon-random', 'url': '/TransaccionesFuncionario'})
    menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
    
    usuariosNoHabilitados = []
    
    with con:    
        cur = con.cursor()
        
        cur.execute('''SELECT identificacion, nombre, mail, usuario
                    FROM Banco.usuario
                    WHERE autorizado_por is null
                    AND tipo_usuario = 2;''',())
                        
        usuarios = cur.fetchall()     
    
    for x in usuarios:
        usuariosNoHabilitados.append({'identificacion': x[0], 'nombre': x[1], 'mail': x[2], 'usuario': x[3]})
    
        
    
    return render_template('habilitarUsuarios.html', menu=menu, usuarios=usuariosNoHabilitados)
    
@app.route("/habilitarUsuario", methods=['POST'])
def habilitarUsuario():
    
    
    if 'perfil' not in session or session['perfil'] != 1:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')
        
        
    if request.method == 'POST':
        identificacion = request.form['identificacion']
        nombre = request.form['nombre']
        mail = request.form['mail']
        
        # Se habilita el usuario en la plataforma        
        with con:    
            cur = con.cursor()
            cur.execute('''UPDATE Banco.usuario set autorizado_por = %s where identificacion = %s; ''', (session['user'], identificacion))
            cur.fetchall()                
            con.commit()        
        
        # Se crea la cuenta bancaria
        numero_cuenta = '0'
        while(numero_cuenta == '0'):
            numeroCuenta = str(uuid.uuid1())
            
            # Se verifica que no exista
            with con:    
                cur = con.cursor()
                cur.execute('''select * from Banco.cuenta where codigo = %s; ''', (numeroCuenta))
                data = cur.fetchall()                
            
            if len(data) == 0:
                
                
                cur = con.cursor()
                cur.execute('''insert into Banco.cuenta values (%s, %s, 10000); ''', (numeroCuenta, identificacion))
                data = cur.fetchall()                
                con.commit()
                
                numero_cuenta = numeroCuenta
            
        # Se generan los 100 códigos únicos y aleatorios
        numero_codigos = 0    
        codigos = []
        while(numero_codigos < 100):
            numero = str(uuid.uuid4())
            
            # Se verifica que no exista
            cur = con.cursor()
            cur.execute('''select * from Banco.codigo_transaccion where codigo = %s; ''', (numero))
            data = cur.fetchall()                            
            
            if len(data) == 0:
                codigos.append(numero)
                cur = con.cursor()
                cur.execute('''insert into Banco.codigo_transaccion values (%s, %s, '0'); ''', (numero, identificacion))
                data = cur.fetchall()                
                con.commit()                
                
                numero_codigos = numero_codigos + 1
                    
        
        # Se envían los códigos al email        
        remitente = 'No-reply <bancoseguro2016@gmail.com>'  
        destinatario = nombre + ' <' + mail + '>'
        asunto = 'Bienvenido al Banco Seguro'  
        
        codigos_string = "\n".join(codigos)        
        
        mensaje = '''   Muchas gracias por registrarse en el banco Seguro \n
                        El número de su cuenta es el siguiente: \n
                        ''' + numero_cuenta + '''\n
                        Por otro lado, los códigos para realizar transacciones son los siguientes:\n
                        ''' + codigos_string
                                
        #mensaje = ''' <h2>Muchas gracias por registrarse en el banco Seguro </h2> 
        #
        #           <h4> El número de su cuenta es el siguiente: </h4>
        #            <br>
        #            <h3>''' + numero_cuenta + '''</h3>
        #            
        #            <h4> Por otro lado, los códigos para realizar transacciones son los siguientes: </h4>
        #            <h5>''' + codigos_string + '''</h5> '''
                    
                    
                    
        
        
        
        # Host y puerto SMTP de gmail
        gmail = smtplib.SMTP('smtp.gmail.com', 587)    
        
        #Protocolo de cifrado de datos utilizado por gmail
        gmail.starttls()
        
        #Credenciales
        nombre = 'parametros.txt'
        archivo=open(nombre, 'r')	#observese la “r”
        salida = archivo.readlines()	#le ordenamos leer el archivo
        archivo.close()
        
        correoGlobal = salida[3].replace('\n', '')
        claveGlobal = salida[4].replace('\n', '')
        
        gmail.login(correoGlobal,claveGlobal)
        
        # Muestra la operacion de depuracion de envio 1=true
        #gmail.set_debuglevel(1)
        
        header = MIMEMultipart()
        header['Subject'] = asunto
        header['From'] = remitente
        header['To'] = destinatario
        mensaje = MIMEText(mensaje, 'plain') # Content-type:text/html
        header.attach(mensaje)
        
        enviado = False
        numero_intentos = 0
        while enviado == False and numero_intentos < 3:
            try:
                # Enviar email
                gmail.sendmail(remitente, destinatario, header.as_string())
                enviado = True
                break
            except:
                numero_intentos = numero_intentos + 1
                print sys.exc_info()[0]
        
        # Cerrar conexion SMTP
        gmail.close()
    
        
        
    return redirect(url_for('pageHabilitarUsuarios'))
    
    
    
@app.route("/transferirDinero")
def transferirDinero():
    
    if 'perfil' not in session or session['perfil'] != 2:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')    
    
    menu = []
    menu.append({'tipo': 'app', 'valor': 'Transferir dinero', 'icono': 'glyphicon-random', 'url': '/transferirDinero'})
    menu.append({'tipo': 'app', 'valor': 'Ver transacciones', 'icono': 'glyphicon-blackboard', 'url': '/TransaccionesUsuario'})
    menu.append({'tipo': 'app', 'valor': 'SubirArchivo', 'icono': 'glyphicon-blackboard', 'url': '/subirArchivo'})
    menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
    
    cuentas = []
    cur = con.cursor()
    cur.execute(''' select codigo, saldo from Banco.cuenta where codigoUsuario = '%s';''', (session['identificacion']))
    data = cur.fetchall()       
    
    for x in data:
        cuentas.append({'codigo': x[0], 'saldo': x[1]})   
        
    
    return render_template('transferirDinero.html', menu=menu, cuentas=cuentas)


@app.route("/transferirFondos", methods=['POST'])
def transferirFondos():
    
    if 'perfil' not in session or session['perfil'] != 2:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')    
    
    if request.method == 'POST':
        cuentaOrigen = request.form['cuentaOrigen']
        cantidad = request.form['cantidad']
        cuentaDestino = request.form['cuentaDestino']
        codigo = request.form['codigo']
        token = request.form['token']
        
        
        
        
        my_secret = cuentaOrigen
        
        my_secret = my_secret.replace('0', 'A')        
        my_secret = my_secret.replace('1', 'B')        
        my_secret = my_secret.replace('2', 'C')        
        my_secret = my_secret.replace('3', 'D')        
        my_secret = my_secret.replace('4', 'E')        
        my_secret = my_secret.replace('5', 'F')        
        my_secret = my_secret.replace('6', 'G')        
        my_secret = my_secret.replace('7', 'H')        
        my_secret = my_secret.replace('8', 'I')        
        my_secret = my_secret.replace('9', 'J')        
        my_secret = my_secret.replace('-', '')   
        print(my_secret)
        my_token = otp.get_totp(my_secret)
        
        print(my_token)
        
        print(str(token) != str(my_token))
        if str(token) != str(my_token):
            return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Tu token no corresponde, por favor, intenta de nuevo')        
        
        if cantidad <= 0:
            return render_template('mensaje.html', Encabezado="Ooops", Mensaje="Parece que el valor que estás intentando transferir no es permitido, por favor intenta otro")
        
        # Valida que se tenga el saldo
        cur = con.cursor()
        cur.execute('''select saldo - %s from Banco.cuenta where codigo = %s; ''', (cantidad, cuentaOrigen))
        data = cur.fetchall()
        
        if (data[0])[0] < 0:
             return render_template('mensaje.html', Encabezado="Ooops", Mensaje="Parece que tratas de transferir más de lo que tienes")
        
        # Valida que cuenta destino exista
        cur = con.cursor()
        cur.execute('''select * from Banco.cuenta where codigo = %s; ''', (cuentaDestino))
        data = cur.fetchall()
        
        if len(data) <= 0:
            return render_template('mensaje.html', Encabezado="Ooops", Mensaje="La cuenta destino no existe, por favor verifícala")
        
        
        # Valida que el código corresponda
        cur = con.cursor()
        cur.execute(''' select * from Banco.codigo_transaccion where codigo = %s and estado = '0' and identificacion = %s; ''', (codigo, session['identificacion']))
        data = cur.fetchall()
        
        if len(data) > 0:
            # Valida que el código corresponda
            cur = con.cursor()
            cur.execute(''' update Banco.codigo_transaccion set estado = '1' where codigo = %s and identificacion = %s; ''', (codigo, session['identificacion']))
            data = cur.fetchall()                
            con.commit()               
                
            if int(cantidad) < 10000 and int(cantidad) > 0:
                
                # Se disminuye el dinero de la cuenta origen
                cur = con.cursor()
                cur.execute(''' update Banco.cuenta set saldo = saldo - %s where codigo = %s;''', (cantidad, cuentaOrigen))
                data = cur.fetchall()                
                con.commit()
                
                
                # Se aumenta el dinero de la cuenta destino
                cur = con.cursor()
                cur.execute(''' update Banco.cuenta set saldo = saldo + %s where codigo = %s;''', (cantidad, cuentaDestino))
                data = cur.fetchall()                
                con.commit()
                
            # Se crea registro en la tabla transacciones
            cur = con.cursor()
            cur.execute(''' insert into Banco.transaccion (identificacion, monto, cuenta_origen, cuenta_destino, tipo) values (%s, %s, %s, %s, 1);''', (session['identificacion'], cantidad, cuentaOrigen, cuentaDestino))
            data = cur.fetchall()                
            con.commit()
        
            
            
            return redirect(url_for('transferirDinero'))
        else:
            return render_template('mensaje.html', Encabezado="Ooops", Mensaje="Este codigo no es tuyo o ya fue usado, por favor, usa uno que corresponda")
            
            


@app.route("/TransaccionesUsuario")
def TransaccionesUsuario():
    if 'perfil' not in session or session['perfil'] != 2:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')    
    
    menu = []
    menu.append({'tipo': 'app', 'valor': 'Transferir dinero', 'icono': 'glyphicon-random', 'url': '/transferirDinero'})
    menu.append({'tipo': 'app', 'valor': 'Ver transacciones', 'icono': 'glyphicon-blackboard', 'url': '/TransaccionesUsuario'})
    menu.append({'tipo': 'app', 'valor': 'SubirArchivo', 'icono': 'glyphicon-blackboard', 'url': '/subirArchivo'})
    menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
    
    
    
    transacciones = []
    
    cur = con.cursor()
    cur.execute(''' select identificacion, fecha_hora, monto, cuenta_origen, cuenta_destino, a.saldo, b.saldo
                from Banco.transaccion
                join Banco.tipo_transaccion
                on Banco.transaccion.tipo = Banco.tipo_transaccion.codigo
                join Banco.cuenta a
                on Banco.transaccion.cuenta_origen = a.codigo
                join Banco.cuenta b
                on Banco.transaccion.cuenta_destino = b.codigo
                where a.codigoUsuario = %s
                or b.codigoUsuario = %s;''', (session['identificacion']))
    data = cur.fetchall()
    
    for x in data:        
        
        if x[0] == session['identificacion']:
            transacciones.append({'fecha': x[1], 'monto': x[2], 'cuentaOrigen': x[3], 'cuentaDestino': x[4], 'saldoFinal': x[5], 'tipoTransaccion': 'Crédito (-)'})
        else:
            transacciones.append({'fecha': x[1], 'monto': x[2], 'cuentaOrigen': x[3], 'cuentaDestino': x[4], 'saldoFinal': x[6], 'tipoTransaccion': 'Débito (+)'})
            
        
    
    return render_template('verTransacciones.html', menu=menu, transacciones=transacciones)


@app.route("/TransaccionesFuncionario")
def TransaccionesFuncionario():

    if 'perfil' not in session or session['perfil'] != 1:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')    
    
    menu = []
    menu.append({'tipo': 'app', 'valor': 'Habilitar usuarios', 'icono': 'glyphicon-user', 'url': '/habilitarUsuarios'})
    menu.append({'tipo': 'app', 'valor': 'Ver transacciones a aprobar', 'icono': 'glyphicon-random', 'url': '/TransaccionesFuncionario'})
    menu.append({'tipo': 'app', 'valor': 'Salir', 'icono': 'glyphicon-blackboard', 'url': '/logout'})
    
    
    
    transacciones = []
    
    cur = con.cursor()
    cur.execute(''' select identificacion, fecha_hora, monto, cuenta_origen, cuenta_destino, a.saldo, b.saldo, Banco.transaccion.codigo
                from Banco.transaccion
                join Banco.tipo_transaccion
                on Banco.transaccion.tipo = Banco.tipo_transaccion.codigo
                join Banco.cuenta a
                on Banco.transaccion.cuenta_origen = a.codigo
                join Banco.cuenta b
                on Banco.transaccion.cuenta_destino = b.codigo
                where monto >= 10000
                and autorizado_por is null;''', ())
    data = cur.fetchall()
    
    for x in data:
        
        if x[0] == session['identificacion']:
            transacciones.append({'fecha': x[1], 'monto': x[2], 'cuentaOrigen': x[3], 'cuentaDestino': x[4], 'saldoFinal': x[5], 'id': x[7]})
        else:
            transacciones.append({'fecha': x[1], 'monto': x[2], 'cuentaOrigen': x[3], 'cuentaDestino': x[4], 'saldoFinal': x[6], 'id': x[7]})
            
        
    
    return render_template('aprobarTransacciones.html', menu=menu, transacciones=transacciones)


@app.route("/aprobar", methods=['POST'])
def aprobar():
    if 'perfil' not in session and session['perfil'] != 1:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')    
    
    if request.method == 'POST':
        identificador = request.form['id']
        cuentaOrigen = request.form['cuentaOrigen']
        cuentaDestino = request.form['cuentaDestino']
        monto = int(str(request.form['monto']).split(".")[0])
                        
        
        # Se disminuye el dinero de la cuenta origen
        cur = con.cursor()
        cur.execute(''' update Banco.cuenta set saldo = saldo - %s where codigo = %s;''', (monto, cuentaOrigen))
        cur.fetchall()                
        con.commit()        
        
        # Se aumenta el dinero de la cuenta destino
        cur = con.cursor()
        cur.execute(''' update Banco.cuenta set saldo = saldo + %s where codigo = %s;''', (monto, cuentaDestino))
        cur.fetchall()                
        con.commit()    
        
        cur = con.cursor()
        cur.execute(''' update Banco.transaccion set autorizado_por = %s where codigo = %s; ''', (session['user'], identificador))
        cur.fetchall()                
        con.commit()
        
        return redirect(url_for('TransaccionesFuncionario'))

@app.route("/logout")
def logout():
    
    session.pop('user', None)    
    session.pop('identificacion', None)
    session.pop('perfil', None)
    
    return redirect(url_for('login'))
    
    
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/subirArchivo', methods=['GET', 'POST'])
def upload_file():
    if 'perfil' not in session or session['perfil'] != 2:
        return render_template('mensaje.html', Encabezado='Buen día', Mensaje='Los privilegios con los que accede a la aplicación no son suficientes, si cree que se trata de un error por favor contacte al administrador del sistema')
        
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            print('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            print('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'Subido exitosamente'#redirect(url_for('uploaded_file', filename=filename))
    print(UPLOAD_FOLDER)
    
    os.system(UPLOAD_FOLDER + '\transacciones_batch')
    
    print('se ejecutó')
    
    
    
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''
@csrf.error_handler
def csrf_error(reason):
    return render_template('mensaje.html', Encabezado="Oops", Mensaje=reason), 400
    
    
if __name__ == "__main__":
    app.run(host='0.0.0.0')



